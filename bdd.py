#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sqlite3,os
from misc import isInt

# Create de tabla que almacena los registros de personas en la bbdd
qry_create_registro='''
CREATE TABLE registro(
    correo text,
    nombre text, 
    run integer, 
    genero text
)'''
# Create de indice para el correo en tabla de registro de personas
qry_create_index_registro='''CREATE INDEX idx_registro_correo ON registro(correo)'''
# Create de tabla que almacena los registros de medidas en la bbdd
qry_create_registroMedidas='''
CREATE TABLE registroMedidas (
    correo text,
    peso float, 
    altura float,
    imc float,
    estadoNutricional text,
    fechaRegistro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)'''
# Create de indice para el registro de medidas sobre el correo
qry_create_index_registroMedidas='''CREATE INDEX idx_registroMedidas_correo ON registroMedidas(correo)'''
# Inicializa una base de datos en Sqlite3, si no existen los archivos los crea y crea las tablas requeridas
def dbbInit():
    # Si no existe el archivo de registro, se crea
    if not os.path.isfile('./registro.db'):
        con = sqlite3.connect('registro.db')
        cur = con.cursor()
        cur.execute(qry_create_registro)
        cur.execute(qry_create_index_registro)
        cur.execute(qry_create_registroMedidas)
        cur.execute(qry_create_index_registroMedidas)
        con.commit()
        con.close()
# Valida si un correo existe en la base de datos True existe, False no existe
def dbbExistMail(correo):
    # Se valida la existencia de el en la base de datos
    try:
        con = sqlite3.connect('registro.db')
        cur = con.cursor()
        query = "select coalesce(sum(1),0) from registro where correo = '"+correo+"'"
        cur.execute(query)
        value = cur.fetchone()[0]
        if isInt(value):
            if int(value)==0:
                return(False)
            else:
                return(True)
    except sqlite3.Error as er:
        print("excepcion")
        print('SQLite error: %s' % (' '.join(er.args)))
        print("Exception class is: ", er.__class__)
        print('SQLite traceback: ')
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))
    con.close()
# Inserta un registro de persona en la base de datos, retorna True en caso afirmativo, False en caso de error
def dbbInsertRegister(correo,nombre,run,genero):
    try:
        con = sqlite3.connect('registro.db')
        cur = con.cursor()
        cur.execute("INSERT INTO registro (correo,nombre,run,genero) VALUES (?,?,?,?)",(correo,nombre,run,genero))
        con.commit()
        con.close()
        return(True)
    except:
        return(False)
# Inserta un registro de medidas, retorna True en caso afirmativo, False en caso de error
def dbbInsertMeasure(correo,peso,altura,imc,estadoNutricional):
    try:
        con = sqlite3.connect('registro.db')
        cur = con.cursor()
        cur.execute("INSERT INTO registroMedidas (correo,peso,altura,imc,estadoNutricional) VALUES (?,?,?,?,?)",(correo,peso,altura,imc,estadoNutricional))
        con.commit()
        con.close()
        return(True)
    except:
        return(False)
# retorna el genero asociado a un mail
def dbbGetGender(correo):
    try:
        con = sqlite3.connect('./registro.db')
        cur = con.cursor()
        query = "select genero from registro where correo = '"+correo+"'"
        cur.execute(query)
        value = cur.fetchone()[0]
        con.close()
        return(str(value))
    except sqlite3.Error as er:
        print("excepcion")
        print('SQLite error: %s' % (' '.join(er.args)))
        print("Exception class is: ", er.__class__)
        print('SQLite traceback: ')
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))
# returna el run asociado a un mail
def dbbGetRun(correo):
    try:
        con = sqlite3.connect('./registro.db')
        cur = con.cursor()
        query = "select run from registro where correo = '"+correo+"'"
        cur.execute(query)
        value = int(cur.fetchone()[0])
        con.close()
        return(value)
    except sqlite3.Error as er:
        print("excepcion")
        print('SQLite error: %s' % (' '.join(er.args)))
        print("Exception class is: ", er.__class__)
        print('SQLite traceback: ')
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))