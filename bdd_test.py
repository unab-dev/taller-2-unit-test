#!/usr/bin/python
# -*- coding: UTF-8 -*-
import unittest
from bdd import dbbInsertRegister,dbbExistMail,dbbGetGender,dbbGetRun

class TestInsertMailInDBB(unittest.TestCase):
    def test_dbbInsertRegister(self):
        # Prueba que se puedan realizar las acciones esperadas en registrarPersona
        self.assertEqual(dbbInsertRegister("foo.bar@baz.cl","foo",1234,"masculino"),True)
class TestExistMailInDBB(unittest.TestCase):
    def test_dbbExistMail(self):
        # Prueba que se puedan realizar las acciones esperadas en registrarPersona
        self.assertEqual(dbbExistMail("foobarbaz"),False)
        self.assertEqual(dbbExistMail("foo.bar@baz.cl"),True)
class TestdbbGetGender(unittest.TestCase):
    def test_dbbGetGender(self):
        self.assertEqual(dbbGetGender("foo.bar@baz.cl"),"masculino")
class TestGetRun(unittest.TestCase):
    def test_dbbGetRun(self):
        self.assertEqual(dbbGetRun("foo.bar@baz.cl"),1234)
        