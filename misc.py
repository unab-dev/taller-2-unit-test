#!/usr/bin/python
# -*- coding: UTF-8 -*-
import re

regexCorreo = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
regexInt = '^[0-9]*$'
regexFloat = '^[0-9]*([.][0-9]*)?$'
# ---------------
# Funciones miselaneas, incluye:
# isFloat(value) : Retorna True si el valor es Float, False si no lo es
# isInt(value) : Retorna True si el valor es Integer, False si no lo es
# isMail(value) : Retorna True si el valor es una sintáxis de correo, False si no lo es
# isGender(value) : Retorna True si el valor es un género válido cómo: "m","masculino","f","femenino"
# isRun(value) : Retorna True si el valor es un run válido (criterio : al menos 4 digitos)
# isText(value) : Retorna True si el valor es solo texto, False si no
# getMailUser(value) : Retorna el usuario para una cuenta de correo
# --------
# isFloat(value) : Retorna True si el valor es Float, False si no lo es
def isFloat(value):
    if(re.search(regexFloat,str(value))):
        return(True)
    else:
        return(False)
# isInt(value) : Retorna True si el valor es Integer, False si no lo es
def isInt(value):
    if(re.search(regexInt,str(value))):
        return(True)
    else:
        return(False)
# isMail(value) : Retorna True si el valor es una sintáxis de correo, False si no lo es
def isMail(value):
    if(re.search(regexCorreo, str(value))):
        return(True)
    else:
        return(False)
# isGender(value) : Retorna True si el valor es un género válido cómo: "m","masculino","f","femenino"
def isGender(value):
    if value.lower() in ["m","masculino","f","femenino"]:
        return(True)
    else:
        return(False)
# isRun(value) : Retorna True si el valor es un run válido (criterio : al menos 4 digitos)
def isRun(value):
    if len(str(value))>3:
        return(True)
    else:
        return(False)
# isText(value) : Retorna True si el valor es solo texto, False si no
def isText(value):
    if(re.search('^[a-zA-Z ]*$',str(value))):
        return(True)
    else:
        return(False)
# getMailUser(value) : Retorna el usuario para una cuenta de correo
def getMailUser(value):
    aux=""
    for i in value:
        if(i == '@'):
            break
        aux=aux+i
    return(aux)