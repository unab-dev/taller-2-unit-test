#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
import sqlite3
import pandas as pd
from consolemenu import SelectionMenu
# -- desarrollo de este proyecto:
from misc import isMail,isFloat,isInt
from bdd import dbbInit,dbbExistMail,dbbGetRun,dbbGetGender,dbbInsertMeasure
from taller2 import registrarPersona,calcularIMC,mostrarEstadoNutricional,calcularContrasena


listMenuPrincipal = ["Registrar","Ingresar"]
nombreMenuPrincipal = "> iMC > inicio"
listMenuUsuario = ["Calcular IMC y Estado Nutricional","Ver registro historico"]
nombreMenuUsuario = "> iMC > Usuario : "
mnuPrincipal = SelectionMenu(listMenuPrincipal,nombreMenuPrincipal)

# Muestra en consola el histórico de pesos para un correo especifico
def cuadriculaRegistroHistorico(correo):
    conn = sqlite3.connect('registro.db')
    print("registro historico:")
    print("=====================\n")
    # print("nombre : "+dbbGetName)
    print("run : "+str(dbbGetRun(correo)))
    print("genero : "+dbbGetGender(correo)+"\n"")
    query = "SELECT fechaRegistro,peso,altura,imc,estadoNutricional FROM registroMedidas WHERE correo = '"+correo+"' order by fechaRegistro desc"
    print (pd.read_sql_query(query,conn))
    conn.close()
    input("\n\npresione enter para continuar...")
# Muestra el formulario para realizar cálculo de IMC
def formularioCalculoIMC(correo):
    print("calculo IMC:")
    print("=====================\n")
    peso = input("Ingrese su peso en \"kg\" (ej 72.5) : ")
    while not isFloat(peso) or (isFloat(peso) and float(peso)<=0):
        if isFloat(peso)==False:
            print("\nFavor ingresar su peso en kilogramos, ej: si uste pesa \"64,5 kilogramos\", debe ingresar \"64.5\".")
        if (isFloat(peso) and float(peso)<=0):
            print("\nSu peso no puede ser negativo")
        peso = input("\nIngrese su peso en \"kg\" (ej 72.5) : ")
    altura = input("Ingrese su altura en metros (ej 1.73)  : ")
    while not isFloat(altura) or (isFloat(altura) and float(altura)<=0):
        if isFloat(altura)==False:
            print("\nFavor ingresar su altura en metros, ej: si uste misw \"1,75 metros\", debe ingresar \"1.75\".")
        if (isFloat(altura) and float(altura)<=0):
            print("\nSu altura no puede ser negativa")
        altura = input("\nIngrese su altura en metros (ej 1.73)  : ")
    imc = calcularIMC(float(peso),float(altura))
    print("-----------------------")
    print("\nCálculo de IMC : "+str(imc))
    genero = dbbGetGender(correo)
    estadoNutricional = mostrarEstadoNutricional(imc,genero)
    print("Estado nutricional : "+estadoNutricional)
    opc = "init"
    while opc not in ["Si","No"]:
        try:
            opc = input("\nDesea guardar su estado nutricional? (Si/No) [Default Si]: ")
            if opc=="":
                opc="Si"
        except:
            opc = "Si"
    
    if opc=="Si":
        if dbbInsertMeasure(correo,float(peso),float(altura),float(imc),estadoNutricional):
            print("registro almacenado")
        else:
            print("error al registrar")
    else:
        print("presione cualquier tecla para continuar")
# Menu para realizar el registro de una persona
def formularioRegistro():
    print("Registro de usuarios:")
    print("=====================\n\n")
    correo = input("Ingrese su correo electronico : ")
    while isMail(correo)==False or dbbExistMail(correo)==True:
        print("\nCorreo invalido o existente")
        correo = input("\nIngrese su correo electronico : ")
    nombre = input("Ingrese su nombre : ")
    run = input("Ingrese su run (sin puntos ni dv) : ")
    while isInt(run) == False or len(str(run))<4:
        if isInt(run)==False:
            print("\nEl Rol Unico Nacional debe ser ingresado sin puntos ni dv, ej: si run es \"15.638.245-0\" entonces ingresar \"15638245\"")
        if len(str(run))<4:
            print("\nEl run no puede tener menos de 4 digitos")
        run = int(input("\nIngrese su run (sin puntos ni dv) : "))    
    genero = input ("Selecione su genero (\"m\"=masculino ó \"f\"=femenino) : ")
    while (genero.lower() not in ["m","f"]):
        print("\nFavor seleccione una opcion valida, \"m\" para masculino o \"f\" para femeninon")
        genero = input ("Selecione su genero (\"m\"=masculino, \"f\"=femenino) : ")
    if genero == "m":
        genero = "masculino"
    else:
        genero = "femenino"
    if registrarPersona (correo,nombre, run, genero):
        print("\nusuario registrado correctamente\n")
    else:
        print("\nerror, no se pudo registrar al usuario\n")
    input("\npresione enter para continuar...")
# Menu de autentificación de una persona
def formularioLogin():
    print("Login:")
    print("=====================\n\n")
    correo = input("Usuario : ")
    while not isMail(correo) or not dbbExistMail(correo):
        print("\nEl correo ingresado no existe o tiene un formato inadecuado")
        correo = input("\nUsuario : ")
    run = dbbGetRun(correo)
    clave = input("password : ")
    if calcularContrasena(correo,run)!=clave:
        print("\ncontraseña incorrecta\n")
    else:
        print("\nOK\n")
        menuUsuario(correo)
    input("\nSaliendo a menu principal, presione enter para continuar...")
# Funcion secundaria, mantiene vivo el menu mientras no se decida salir del menu de usuario
def menuUsuario(correo):
    menuUsuario = SelectionMenu(listMenuUsuario,nombreMenuUsuario+correo)
    while True:
        menuUsuario.show()
        selection = menuUsuario.selected_option
        if selection == 0:
            formularioCalculoIMC(correo)
        elif selection == 1:
            cuadriculaRegistroHistorico(correo)
        elif selection == 2:
            break
# Función principal que mantiene vivo el menu mientras no se decida salir de la aplicación
def menuPrincipal():
    dbbInit()
    while True:
        mnuPrincipal.show()
        selection = mnuPrincipal.selected_option
        if selection == 0:
            formularioRegistro()
        elif selection == 1:
            formularioLogin()
        elif selection == 2:
            break

menuPrincipal()