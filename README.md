# Taller 2 - Unit Test
## _Introducción_

>El objetivo de este trabajo, es prácticar la técnica de test unitario vista en clase, por medio de un caso práctico, con base en el desarrollo de una aplicación con la cuál estámos familiarizados, según el documento adjunto.

La ejecución de la unit test o del aplicativo, genera un archivo registro.db en la ruta de ejecución, este archivo es una bbdd Sqlite, por lo que de requerir volver a la configuración inicial de la aplicación, se puede eliminar este archivo. La aplicación determina si existe o si no lo creará junto con la estructura de tablas requerida.

### Que incluye esta entrega - _Release 06_

- Aplicación final : _Menu funcional_
- Separación de funcionalidades : 
  - **Base de datos** : bdd.py y bdd_test.py
  - **Funciones de apoyo** : misc.py y misc_test.py
  - **Funciones del taller** : taller2.py y taller2_test.py
  - **Menú** : menu.py

### Librerias requeridas
Para la ejecución de esta aplicación, es requerido contar con las siguientes librerías en Python 3.7

- sqlite3
- pandas
- consolemenu
- unittest

### Ejecutar la aplicación

El código fue validado con **python 3.7** 

#### Test

Cada modulo contiene su propio archivo de pruebas, para ejecutar, se debe ejecutar:

```
python -m unittest <nombre_archivo_test_sin_extensión_py>
```

Donde el <nombre_archivo_test_sin_extension_py> es:
- Base de datos : **bdd_test**
- Funciones de apoyo : **misc_test**
- Funciones del taller : **taller2_test**

#### Menu

Para ejecutar el menú se debe ejecutar:
```
python menu.py
```

### Que incluye esta entrega - _Release 05_

#### taller2.py

Incluye la función **calcularContraseña()**

#### taller2_test.py

- se valida que el calculo de contraseña sea el esperado
- se valida que el calculo de contraseña considere las excepciones por tipo y valor 

#### Uso de Unit Test

**se valida que el desarrollo cumple los requisitos**

img_006.png


### Que incluye esta entrega - _Release 04_

#### taller2.py

Incluye la función **mostrarEstadoNutricional()**

#### taller2_test.py

- se valida que el calculo de estado nutricional sea el correcto
- se valida que se muestren errores de tipo y valor

#### Uso de Unit Test

**se valida que el desarrollo cumple los requisitos**

img_005.png

### Que incluye esta entrega - _Release 03_

#### taller2.py

Se incluye la función **calcularIMC()**

#### taller2_test.py

- se valida que el cálculo de IMC sea el correcto
- se valida que los tipos de datos de entrada sean enteros o flotantes

#### Uso de Unit Test

**se valida que el desarrollo cumple los requisitos**

img_004.png


### Que incluye esta entrega - _Release 02_

#### taller2.py

- validarCorreo : valida si un correo ya existe en la base de datos
- registrarPersona: se implementa función validarCorreo para no duplicar correos

#### taller2_test.py

- se añaden métodos para hacer pruebas unitarias a validarCorreo
- se resuelve incidente de correos duplicados sin error de valor

#### Uso de Unit Test

- validarCorreo: se valida que la función validarCorreo detecte correos válidos por pre existencia en bbdd
- se valida que la función validarCorreo detecte correos inválidos por tipo de dato
- evidencia ejecución img_003.png


### Que incluye esta entrega - _Release 01_

#### taller2.py
- main() : inicializa una tabla en sqlite para registrar personas
- registrarPersona() : recibe como parámetros el correo, nombre, run y genero de una personas para realizar registro

#### taler2_test.py
- test_registrarPersona :
 - evalúa el registro de 2 personas con distinto correo, nombre en texto, run en digitos y genero en distinta forma de expresión

- test_value : 
 - evalúa insertar una persona con run en formato texto
 - evalúa insertar una persona con un email ya insertado

#### Uso de Unit Test

 Se debe ejecutar en la ruta del proyecto:

 python -m unittest taller2_test

 en esta ejecución se recibió de respuesta un error debido a que la función no arrojó ninguna excepción al momento de insertar un run con texto ni al registrar una persona existente en la base de datos.

 se adjunta img_001.png como evidencia del mensaje

 además se valida vía consulta bbdd que se insertaron los registros válidos de la prueba img_002.png