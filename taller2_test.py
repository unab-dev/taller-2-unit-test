#!/usr/bin/python
# -*- coding: UTF-8 -*-
import unittest
from taller2 import registrarPersona,calcularIMC,mostrarEstadoNutricional,calcularContrasena

class TestRegistrarPersona(unittest.TestCase):
    def test_registrarPersona(self):
        # Prueba que se puedan realizar las acciones esperadas en registrarPersona
        self.assertEqual(registrarPersona("foo@bar.cl","foo",1234,"masculino"),True)
        self.assertEqual(registrarPersona("foo@bar.com","foo",1234,"masculino"),True)
    def test_registrarPersona_values(self):
        # Prueba que se eleven errores cuando es necesario en funcion registrarPersona
        # correo repetido
        self.assertRaises(ValueError, registrarPersona,"foo@bar.cl","foo",1234,"masculino")
        # run menor a 3 digitos
        self.assertRaises(ValueError, registrarPersona,"foo@bar.baz","foo",123,"masculino")
        # genero
        self.assertRaises(ValueError, registrarPersona,"foo@bar.baz","foo",1234,"no binario")
    def test_registrarPersona_types(self):
        # Prueba que se eleven los errores de tipo de dato cuando es necesario
        # correo electronico
        self.assertRaises(TypeError, registrarPersona,1234,"foo",1234,"masculino") 
        self.assertRaises(TypeError, registrarPersona,"foo bar","foo",1234,"masculino") 
        self.assertRaises(TypeError, registrarPersona,"foo@bar","foo",1234,"masculino") 
        self.assertRaises(TypeError, registrarPersona,"foo@bar.","foo",1234,"masculino") 
        # run no numerico
        self.assertRaises(TypeError, registrarPersona,"baz@bar.foo","name","run","masculino")
        # genero numérico
        self.assertRaises(TypeError, registrarPersona,"baz@bar.foo","name",1234,1234)
class TestCalcularIMC(unittest.TestCase):
    def test_calcularIMC(self):
        # Prueba de que se calcula el IMC
        self.assertEqual(calcularIMC(1,1),1)
        self.assertEqual(calcularIMC(4,2),1)
        self.assertEqual(calcularIMC(8,2),2)
        self.assertAlmostEqual(calcularIMC(84.5,1.65),31.0376492195)
    def test_calcularIMC_type(self):
        self.assertRaises(TypeError, calcularIMC,"foo",1)
        self.assertRaises(TypeError, calcularIMC,1,"bar")
    def test_calcularIMC_value(self):
        self.assertRaises(ValueError,calcularIMC,0,1)
        self.assertRaises(ValueError,calcularIMC,1,0)
        self.assertRaises(ValueError,calcularIMC,0,0)
class TestMostrarEstadoNutricional(unittest.TestCase):
    def test_mostrarEstadoNutricional(self):
        self.assertAlmostEqual(mostrarEstadoNutricional(24.5,"m"),"Normal")
        self.assertAlmostEqual(mostrarEstadoNutricional(24.5,"femenino"),"Obesidad Leve")
    def test_mostrarEstadoNutricional_type(self):
        self.assertRaises(TypeError,mostrarEstadoNutricional,"foo","m")
        self.assertRaises(TypeError,mostrarEstadoNutricional,22.4,12.1)
    def test_mostrarEstadoNutricional_value(self):
        self.assertRaises(ValueError,mostrarEstadoNutricional,12.4,"bar")
        self.assertRaises(ValueError,mostrarEstadoNutricional,0,"masculino")
class TestCalcularContrasena(unittest.TestCase):
    def test_calcularContrasena(self):
        self.assertAlmostEqual(calcularContrasena("foo@bar.cl",123456),"foo1234")
    def test_calcularContrasena_type(self):
        self.assertRaises(TypeError,calcularContrasena,123,12345)
        self.assertRaises(TypeError,calcularContrasena,"foo@bar.cl","run")
    def test_calcularContrasena_value(self):
        self.assertRaises(ValueError,calcularContrasena,"foo@bar.cl",123)
