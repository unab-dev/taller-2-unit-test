#!/usr/bin/python
# -*- coding: UTF-8 -*-
import unittest
from misc import isFloat,isInt,isMail,isGender,isRun,isText,getMailUser

class TestIsFloat(unittest.TestCase):
    def test_func_isFloat(self):
        self.assertEqual(isFloat("foo"),False)
        self.assertEqual(isFloat("12,5"),False)
        self.assertEqual(isFloat("12.5"),True)
        self.assertEqual(isFloat(12.5),True)
        self.assertEqual(isFloat(12),True)
class TestIsInt(unittest.TestCase):
    def test_func_isInt(self):
        self.assertEqual(isInt("foo"),False)
        self.assertEqual(isInt("12.5"),False)
        self.assertEqual(isInt(11.5),False)
        self.assertEqual(isInt("11"),True)
        self.assertEqual(isInt(12),True)
class TestIsMail(unittest.TestCase):
    def test_func_isMail(self):
        self.assertEqual(isMail(12.5),False)
        self.assertEqual(isMail("12.5"),False)
        self.assertEqual(isMail("foo bar"),False)
        self.assertEqual(isMail("foo@bar"),False)
        self.assertEqual(isMail("foo@bar.cl"),True)
        self.assertEqual(isMail("foo@bar-baz.cl"),True)
        self.assertEqual(isMail("foo.1234_bar@baz_1234-baf.cl"),True)
        self.assertEqual(isMail("luis.test@test.cl"),True)
class TestIsGender(unittest.TestCase):
    def test_func_isGender(self):
        self.assertEqual(isGender("foo"),False)
        self.assertEqual(isGender("m"),True)
        self.assertEqual(isGender("masculino"),True)
        self.assertEqual(isGender("f"),True)
        self.assertEqual(isGender("femenino"),True)
class TestIsRun(unittest.TestCase):
    def test_func_isRun(self):
        self.assertEqual(isRun(123),False)
        self.assertEqual(isRun(1234),True)
        self.assertEqual(isRun(12345678),True)
class TestIsText(unittest.TestCase):
    def test_func_isText(self):
        self.assertEqual(isText("123"),False)
        self.assertEqual(isText(1234),False)
        self.assertEqual(isText("foo 123 bar"),False)
        self.assertEqual(isText("foo bar"),True)
class TestGetMailUser(unittest.TestCase):
    def test_func_getMailUser(self):
        self.assertEqual(getMailUser("foo@bar.cl"),"foo")
        self.assertEqual(getMailUser("foo.bar@baz.cl"),"foo.bar")
