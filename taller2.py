#!/usr/bin/python
# -*- coding: UTF-8 -*-
import re,os
from misc import isFloat,isInt,isMail,isRun,isGender,isText,getMailUser
from bdd import dbbInit,dbbExistMail,dbbInsertRegister

# Cuando se deje de desarrollar, se debe modificar el valor o eliminar la siguiente función
# En caso de encontrarse en ambiente de desarrollo, elimina el archivo de registro de existir.
dev = 0
if dev==1:
    if os.path.isfile('./registro.db'):
        os.remove('./registro.db')
# Inicializa la base de datos
dbbInit()

# validarCorreo () : 
# Valida que un correo tenga el formato adecuado 
# --
# Para este release:
# - Existe una función auxiliar que valida si el registro es mail "isMail", tiene su propio archivo de pruebas unitarias
# - Además existe una función de base de datos que busca si el correo existe en la base de datos "dbbExistMail", tiene su propio archivo de pruebas unitarias
# ---

# Registra una persona en la base de registro.db sqlite3, True si la puede insertar, False si no es posible
def registrarPersona(correo,nombre,run,genero):
    # Valida que el correo tenga la sintaxis adecuada
    if not isMail(correo):
        raise TypeError("El correo no cuenta con la sintaxis adecuada")
    # Se valida que el correo no exista en la base de datos
    if dbbExistMail(correo):
        raise ValueError("El correo indicado ya existe")
    # Se valida que el run venga en el tipo de datos adecuado
    if not isInt(run):
        raise TypeError("El run no debe incluir puntos, ni guión o digito verificador")
    if not isRun(run):
        raise ValueError("El run debe contener al menos cuatro digitos")
    # se valida que el genero venga en el tipo de datos adecuado
    if not isText(genero):
        raise TypeError("El genero debe ser un texto") 
    # se valida que el valor sea el adecuado
    if not isGender(genero):
        raise ValueError ("El genero debe ser \"m\", \"masculino\", \"f\" o \"femenino\"")
    return(dbbInsertRegister(correo,nombre,run,genero))
# Realiza el cálculo del IMC en función de dos valores
def calcularIMC(peso, altura):
    if not isFloat(peso):
        raise TypeError("El peso debe ser de tipo numerico")
    elif not isFloat(altura):
        raise TypeError("La altura debe ser de tipo numerico")
    elif (peso<=0 or altura <=0):
        if peso<=0:
            raise ValueError("El peso debe ser mayor a cero")
        else:
            raise ValueError("La altura debe ser mayor a cero")
    else:
        return (peso/(altura**2))
# Muestra el estado nutricional segun un imc y un genero
def mostrarEstadoNutricional (imc, genero):
    if not isFloat(imc):
        raise TypeError("El IMC debe ser un valor numerico")
    elif not isText(genero):
        raise TypeError("El genero debe ser un texto")
    elif imc <= 0:
        raise ValueError("El IMC debe ser un valor mayor a 0")
    elif not isGender(genero):
        raise ValueError("Genero no identificado")
    elif imc < 20:
        return("Bajo Peso")
    else:
        if genero in ["m","masculino"]:
            if 20<=imc<25:
                return("Normal")
            elif 25<=imc<30:
                return("Obesidad Leve")
            elif 30<=imc<40:
                return("Obesidad Severa")
            else:
                return("Obesidad muy severa")
        else:
            if 20<=imc<24:
                return("Normal")
            elif 24<=imc<29:
                return("Obesidad Leve")
            elif 29<=imc<37:
                return("Obesidad Severa")
            else:
                return("Obesidad muy severa")
# Realiza el cálculo de una contraseña para un correo válido y un rut válido
def calcularContrasena(correo,run):
    if isFloat(correo):
        raise TypeError("El correo no utiliza el tipo de datos adecuado")
    if not isMail(correo):
        raise ValueError("El correo se encuentra en un formato inadecuado")
    elif not isInt(run):
        raise TypeError("El run debe ser de tipo numerico")
    elif len(str(run)) < 4:
        raise ValueError("Para calcular constrasena se requieren al menos 4 digitos en el run")
    else:
        return(getMailUser(correo)+str(run)[0:4])
    